use criterion::{black_box, criterion_group, criterion_main, Criterion};

const DATALEN: usize = 498;

mod sha {
    use super::*;

    fn sha_1(c: &mut Criterion) {
        use sha1::{Digest, Sha1};
        let mut sha1 = Sha1::new();
        let data = [0_u8; DATALEN];
        c.bench_function("sha-1::sha1", |b| {
            b.iter(|| {
                sha1.update(black_box(&data));
                sha1.clone().finalize()
            })
        });
    }

    fn sha1(c: &mut Criterion) {
        use sha1_no_hyphen::{Digest, Sha1};
        let mut sha1 = Sha1::new();
        let data = [0_u8; DATALEN];
        c.bench_function("sha1::sha1", |b| {
            b.iter(|| {
                sha1.update(black_box(&data));
                sha1.clone().finalize()
            })
        });
    }

    fn ring(c: &mut Criterion) {
        use ring::digest::{Context, SHA1_FOR_LEGACY_USE_ONLY as SHA1};
        let mut sha1 = Context::new(&SHA1);
        let data = [0_u8; DATALEN];
        c.bench_function("ring::sha1", |b| {
            b.iter(|| {
                sha1.update(black_box(&data));
                sha1.clone().finish()
            })
        });
    }

    fn openssl(c: &mut Criterion) {
        use openssl::sha::Sha1;
        let mut sha1 = Sha1::new();
        let data = [0_u8; DATALEN];
        c.bench_function("openssl::sha1", |b| {
            b.iter(|| {
                sha1.update(black_box(&data));
                sha1.clone().finish()
            })
        });
    }

    criterion_group!{
        name = sha;
        config = Criterion::default();
        targets = sha_1, sha1, ring, openssl
    }
}

mod aes {
    use super::*;

    fn aes07(c: &mut Criterion) {
        use aes07::Aes128Ctr;
        use cipher03::{NewCipher, StreamCipher};
        let mut cipher = Aes128Ctr::new(
            &[1_u8; 16].into(),
            &[0_u8;16].into());
        let mut data = [0_u8; DATALEN];
        c.bench_function("aes0.7::aes", |b| {
            b.iter(|| {
                cipher.apply_keystream(&mut data);
            })
        });
    }

    fn aes08(c: &mut Criterion) {
        use aes08::{Aes128, cipher::{StreamCipher, KeyIvInit}};
        use ctr::Ctr64LE;
        type C = Ctr64LE<Aes128>;
        let mut cipher = C::new(
            &[1_u8; 16].into(),
            &[0_u8;16].into());
        let mut data = [0_u8; DATALEN];
        c.bench_function("aes0.8::aes", |b| {
            b.iter(|| {
                cipher.apply_keystream(&mut data);
            })
        });
    }

    fn openssl(c: &mut Criterion) {
        use openssl::symm::{Crypter, Cipher, Mode};
        let mut crypter = Crypter::new(
            Cipher::aes_128_ctr(),
            Mode::Encrypt,
            &[1_u8; 16],
            Some(&[0_u8;16])).unwrap();
        let data = [0_u8; DATALEN];
        let mut output = [0_u8; DATALEN];
        c.bench_function("openssl::aes", |b| {
            b.iter(|| {
                crypter.update(&data[..], &mut output).unwrap();
            })
        });
    }

    criterion_group!{
        name = aes;
        config = Criterion::default();
        targets = aes07, aes08, openssl
    }
}

criterion_main!(sha::sha, aes::aes);
